#' @title function to check climate files for consistency with the original data
#' @description currently set up for the dischma CoFoLaMo runs so needs to be
#' tested on other data - maybe this can be removed with testthat
check_SED_files <- function(clim_file_prefix,
                            clim_dir,
                            temp_RTS,
                            prcp_RTS,
                            spin_up) {

  
  sink(file.path(clim_dir, 
                 paste0(clim_file_prefix, 
                        "RTS_and_clim_file_comparison_log.txt")))
  # There are 914 climate files 
  
  # pick 91 numbers at random between 0 and 913
  file_nos <- floor(runif(91, min=0, max=913))
  
  for(i in 1:length(file_nos)) {
  
    # read in the first climate file
    in_file <- read.csv(file.path(clim_dir, 
                                  paste0(clim_file_prefix, "_", file_nos[i], ".txt")), 
                        skip = 5, sep = " ", header = FALSE)
    
    cat("*******************************************************", "\n")
    cat("Current file is: ", paste0(clim_file_prefix, "_", file_nos[i], ".txt"), "\n")
  
    names(in_file) <- c("x", "y", "year",
                        "J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D",
                        "J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D")
    # get the year 
    curr_year <- unique(in_file[,3])
    
    # if the year is < 1930 get the year it corresponds to in the spinup
    if(curr_year<1930) {
      
      curr_year <- subset(spin_up, year==curr_year)$yearOfClimateDataSet
    }
  
    # from that climate file select 10% of the rows at random 
    # there are 3306 rows so 33 rows
    curr_samp <- dplyr::sample_n(in_file, 33)
    
      # for the first row
      for(j in 1:nrow(curr_samp)) {
        
        # get the cell number from the xy coords
        cell_no <- rts::cellFromXY(temp_RTS, c(curr_samp[j,]$x, curr_samp[j,]$y))
  
        # extract the row temp and precip data from the climate file
        tcor_fm_file <- as.vector(curr_samp[j, 4:15])
        prcp_fm_file <- as.vector(curr_samp[j, 16:27])
    
        # extract the tcor from the RTS climate file
        tcor_fm_RTS <- as.vector(extract(temp_RTS, cell_no, as.character(curr_year)))
        
        # proof of failure - keep commented out
        #tcor_fm_RTS <- as.vector(extract(temp_RTS, 100, as.character(curr_year)))
        
        # extract the prcp from the RTS climate file
        prcp_fm_RTS <- as.vector(extract(prcp_RTS, cell_no, as.character(curr_year)))
        
        # proof of failure - keep commented out
        #prcp_fm_RTS <- as.vector(extract(prcp_RTS, 357, as.character(curr_year)))
        
        # conversion of prcp to mm's is done later in origanl code
        prcp_fm_RTS <- prcp_fm_RTS * 10
        
        # data is rounded differently so make the comparison
        # with tolerance to allow for that
        if(isTRUE(all.equal(tcor_fm_file, tcor_fm_RTS, 
                  tolerance = 0.000001, 
                  check.attributes = FALSE))){
          
          cat("X: ", curr_samp[j,]$x,
              "Y: ", curr_samp[j,]$y,
              "Cell #: ", cell_no, 
              "- RTS and climate file TCOR compared: OK", "\n")
      
        } else {
          cat("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *", "\n")
          cat("* X: ", curr_samp[j,]$x,
              "Y: ", curr_samp[j,]$y,
              "Cell #: ", cell_no, 
              "- RTS and climate file TCOR compared: FAILED *", "\n")
          cat("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *", "\n")
          sink()
          stop("FAILURE! See log file in climate files directory", "\n")
        }
        if(isTRUE(all.equal(prcp_fm_file, prcp_fm_RTS, 
                  tolerance = 0.000001, 
                  check.attributes = FALSE))){
          
          cat("X: ", curr_samp[j,]$x,
              "Y: ", curr_samp[j,]$y,
              "Cell #: ", cell_no, 
              "- RTS and climate file PRCP compared: OK", "\n")
          
        } else {
          cat("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *", "\n")
          cat("* X: ", curr_samp[j,]$x,
              "Y: ", curr_samp[j,]$y,
              "Cell #: ", cell_no, 
              "- RTS and climate file PRCP compared: FAILED*", "\n")
          cat("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *", "\n")
          sink()
          stop("FAILURE! See log file in climate files directory", "\n")
        }
        cat("\n")
    }
  }
  sink()
}