#' @title writes a landclim climate file
#' @description  This function takes as input:
#' a climate data frame where each row contains monthly temperature followed by monthly precipitation; 
#' the climate file to be written out to;
#' the latitude of the site
#' a list containing three objects: the monthly lapse rates for temperature, precipitation and the reference station height
#' the metre step that the lapse rates vary 
#' #' @param filename: a path to the description csv file
#' @param 
#' @return
#' @keywords 
#' @author Austin Haffenden
#' @examples \dontrun{
#' 
#' 
write_climate_file_NON_SED <- function(climate_in, file, latitude, lapse_rates, lapse_step = 1) {
  temp_lapse <- lapse_rates[[1]]
  prec_lapse <- lapse_rates[[2]]
  elevation <- lapse_rates[[3]]
  
  con <- file(file, open = "wt")
  writeLines("#header	info#", con)
  writeLines(paste(latitude, "#latitude	used	for	cacluating	drought	index	in	model	bugmann#"), con)
  writeLines(paste(elevation, "#meter	a.s.l. of	climate	station#"), con)
  writeLines("", con)
  writeLines("#temperature	lapse	rate	with	increasing	altitude:	#", con)
  writeLines(paste(temp_lapse, collapse = " "), con)
  writeLines("", con)
  writeLines("#precipitation	lapse	rate	with	increasing	altitude:	#", con)
  writeLines(paste(prec_lapse, collapse = " "), con)																			
  writeLines("", con)
  writeLines(paste(lapse_step, "#meter:	corrections	(see	above)	every	x	meter.	#"), con)
  writeLines("", con)
  writeLines("#	mean	monthly	temperature	?C	total	monthly	precipitation	mm	#", con)
  writeLines("#	jan	feb	marz	april	mai	june	july	aug	sept	oct	nov	dec	jan	feb	marz	april	mai	june	july	aug	sept	oct	nov	dec	#", con)
  # write(t(x), con, ncolumns = ncol(x), append = TRUE)
  for (i in 1:(NROW(climate_in)-1)){
    writeLines(paste(climate_in[i, ], collapse = " "), con)
  }
  writeLines(paste(climate_in[NROW(climate_in), ], collapse = " "), con, sep = "")
  close(con)
}
