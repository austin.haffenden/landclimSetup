#========================================
write_latitude_to_climate_file <- function(file_prefix, no_years, 
                                           climate_path, ref_lats){

  years <- seq(0, no_years-1, 1)
  
  for(i in 1:length(years)) {

    curr_file_path <- file.path(climate_path, paste0(paste(file_prefix, years[i], sep = "_"), ".txt"))
    curr_file <- readLines(curr_file_path,-1)
    #print(paste(ref_lats, "#average latitude of the sites -	used	for	cacluating	drought	index	in	model	bugmann#", sep = " "))
    #curr_file[2]= paste(ref_lats, "#average latitude of the sites -	used	for	cacluating	drought	index	in	model	bugmann#", sep = " ")
    lat_string <- paste(ref_lats, collapse = " ")
    curr_file[2]= as.character(lat_string)
    #print(curr_file)
    writeLines(curr_file, curr_file_path)
    unlink(curr_file)

  }
}
