#' @title Creates teh simulation file
#' @description  Requires a base_SED_ctl.xml file to be in the xml dir
#' @param
#' @param
#' @return NULL
#' @keywords CoFoLaMo, Dischma, spin-up, climate
#' @author Austin Haffenden
#' @example /don't run {}
#'
create_SED_sim_file <- function(sim_simulation_dir, 
                                sim_xml_dir,
                                file_root) {
  
  dir.create(sim_simulation_dir)
  
  base_SED_ctl <- XML::xmlRoot(XML::xmlTreeParse(file.path(sim_xml_dir, "base_SED_ctl.xml")))
  
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["climate"]][["fileNameStructure"]]) <- paste0("../climate/", file_root, "/", file_root, "_{year}.txt")
  
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["speciesAttributes"]]) <- paste0("../xml_input/", file_root, "_species.xml")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["treeInitState"]])     <- paste0("../initialstate/", file_root, "_tree_init.csv")
  
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["landtypePars"]])      <- paste0("../xml_input/landType_noregeneration.xml")
  
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["landMask"]])          <- paste0("../landscape/", file_root, "/","SED_landtype.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["soilDepth"]])         <- paste0("../landscape/", file_root, "/","SED_soildepth.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["slope"]])             <- paste0("../landscape/", file_root, "/","SED_slope.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["aspect"]])            <- paste0("../landscape/", file_root, "/","SED_aspect.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["elevation"]])         <- paste0("../landscape/", file_root, "/", "SED_elevation.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["availableNitrogen"]]) <- paste0("../landscape/", file_root, "/","SED_nitrogen.asc")
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["landtype"]])          <- paste0("../landscape/", file_root, "/","SED_landtype.asc")
  
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["output"]][["path"]]) <- paste0("../output/", file_root)
  XML::xmlValue(XML::xmlChildren(base_SED_ctl)[["iterations"]]) <- 1
  XML::saveXML(base_SED_ctl, file = file.path(sim_simulation_dir, paste0(file_root, "_ctl.xml")))
  
  
}