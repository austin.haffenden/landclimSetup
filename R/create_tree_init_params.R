# creates init file parameters for the species not represented by randomly selecting
# values from a species of the same leaf type
create_tree_init_params <- function(not_base_spp, base_species, leaf_type, tree_data = tree_init_data) {
  
  for(i in 1:length(not_base_spp)) {
    
    if(length(not_base_spp)==0) return(base_species)
    
    curr_spp <- not_base_spp[i]
    leaf_type <- unique(tree_data[tree_data$species==curr_spp,]$leaves)
    rand_base <- base_species[sample( which( base_species$leafHabit==leaf_type), 1),]
    rand_base$name <- not_base_spp[i]
    temp_spp <- rbind(rand_base, base_species)
  }
  return(temp_spp)
}
