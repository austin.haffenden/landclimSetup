# coordinates need to be in WGS84
get_climate_fm_ISIMIP_stk <- function(site, start_year, period_length, temp_stack, precip_stack) {
  
  # extract the temp and precip values: jan to dec for site - if i extract all i should have 120 values
  temp <- raster::extract(temp_stack, spTransform(site, proj4string(temp_stack)))[1,]
  prec <- raster::extract(precip_stack, spTransform(site, proj4string(precip_stack)))[1,]
  
  # section those values to the correct year -matrix will have 120 values
  st_index <- ((start_year-2001)*12)+1
  nd_index <- (st_index+(12*period_length))-1
  
  # section those values to the correct year
  temp <- temp[st_index:nd_index]
  prec <- prec[st_index:nd_index]
  
  # convert to a matrix
  temp <- round(matrix(temp, ncol = 12, byrow = TRUE), 1)
  prec <- round(matrix(prec, ncol = 12, byrow = TRUE), 1)
  
  # return for this is a 24 (+1) column with nrows matching years of run  
  year_climate <- cbind(seq(start_year, by = 1, len = period_length), temp, prec)
  
}