#header	info#
44.4 #latitude	used	for	cacluating	drought	index	in	model	bugmann#
881 #meter	a.s.l. of	climate	station#

#temperature	lapse	rate	with	increasing	altitude:	#
0 0 0 0 0 0 0 0 0 0 0 0

#precipitation	lapse	rate	with	increasing	altitude:	#
0 0 0 0 0 0 0 0 0 0 0 0

1 #meter:	corrections	(see	above)	every	x	meter.	#

#	mean	monthly	temperature	?C	total	monthly	precipitation	mm	#
#	jan	feb	marz	april	mai	june	july	aug	sept	oct	nov	dec	jan	feb	marz	april	mai	june	july	aug	sept	oct	nov	dec	#
2004 2.9 3.2 5.5 9.6 12.6 18.1 19.9 20.2 17.1 13.9 5.9 4 65 73 17.4 60.6 37.1 11.3 7.1 108.3 20.4 182.3 19.8 69.2
2005 2 0.9 5.7 9.4 14.4 19.6 20.7 19.1 16.5 13.1 5.9 0.8 16.2 12 42.1 101.6 45.3 35.1 20.8 28.6 92.6 111.3 53.7 82.1
2006 1.9 2.3 5.9 10.3 14.1 18.5 23.5 18.3 17.8 14.4 9.2 4.4 54.1 49.5 76.2 26.5 40.8 11.3 55.4 45.9 104.5 78.5 56.6 76
2007 4.8 5.5 6.9 12.1 14.6 18 19.5 18.8 15.6 11.5 5.4 3.1 36 64.2 39.7 47.3 108.2 80.5 19.6 40.3 38.4 7.8 122.9 40.1
2008 4.8 4.7 6.3 9.3 14.3 17.8 20.1 19.8 15.4 12 6.5 2.9 115.2 39.9 51.5 119.8 153.6 73.9 37.5 62.2 116.6 87.7 136.7 106.2